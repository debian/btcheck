/*
 * bdecode.c
 *
 * $Id: bdecode.c 26 2012-02-19 22:28:17Z diraison $
 *
 * This file is part of the btcheck project (c) 2008-2009 distributed
 * under the GNU GPLv3 license and created by Jean Diraison
 * <jean.diraison@ac-rennes.fr>
 *
 * URL: http://sourceforge.net/projects/btcheck/
 *
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>

#include "btree.h"
#include "bdecode.h"

static int get_torrent_char(FILE *torrent)
{
	int c;
	
	c = fgetc(torrent);
	if (c == EOF) {
		fprintf(stderr, "EOF prematurely met, file corrupted!\n");
		exit(EXIT_FAILURE);
	}
	return c;
}

static int unget_torrent_char(int c, FILE *torrent)
{
	return ungetc(c, torrent);
}

static integer_t get_torrent_integer(FILE *torrent)
{
	integer_t integer = 0;
	int first = 1;
	int sign = 1;
	int c;
	
	while ((c = get_torrent_char(torrent)) != 'e') {
		if (first) {
			first = 0;
			if (c == '-') {
				sign = -1;
				continue;
			}
		}
		if (c < '0' || c > '9') {
			fprintf(stderr, "Integer format error, file corrupted!\n");
			exit(EXIT_FAILURE);
		}
		integer = 10 * integer + (c-'0');
		if (integer >= MAX_INTEGER_VALUE) {
			fprintf(stderr, "Integer value overflow.\n");
			exit(EXIT_FAILURE);
		}
	}
	integer = (sign < 0 ? -integer : integer);
	// fprintf(stderr, "\t\tinteger = " LLD_FMT "\n", (long long)integer);
	return integer;
}

static string_t get_torrent_string(FILE *torrent, int *stringlength)
{
	unsigned char *string;
	int length = 0;
	int c, i;
	
	while ((c = get_torrent_char(torrent)) != ':') {
		if (c < '0' || c > '9') {
			fprintf(stderr, "String length error, file corrupted!\n");
			exit(EXIT_FAILURE);
		}
		length = 10 * length + (c-'0');
		if (length >= MAX_STRING_LENGTH) {
			fprintf(stderr, "String length overflow.\n");
			exit(EXIT_FAILURE);
		}
	}
	string = malloc(length+1);
	if (string == NULL) {
		perror("Can't allocate memory for string.");
		exit(EXIT_FAILURE);
	}
	for (i = 0; i < length; i++) {
		string[i] = get_torrent_char(torrent);
	}
	string[length] = '\0';
	// fprintf(stderr, "\t\tstring = %s\n", string);
	if (stringlength != NULL)
		*stringlength = (unsigned long) length;
	return (string_t)string;
}

btree_t *bdecode(FILE *torrent)
{
	btree_t *newbtree = NULL;
	int c, ret;
	
	c = get_torrent_char(torrent);
	// fprintf(stderr, "bdecode(%c)\n", c);
	if (c == 'd') {					/* dictionary */
		dictitem_t dictitem;
		int length;
		newbtree = new_btree_dict(NULL, 0);
		if (newbtree == NULL) {
			fprintf(stderr, "Can't create new dictionary.\n");
			exit(EXIT_FAILURE);
		}
		while ((c = get_torrent_char(torrent)) != 'e') {
			unget_torrent_char(c, torrent);
			// fprintf(stderr, "\tdictionary(%c)\n", c);
			dictitem.attribute = get_torrent_string(torrent, &length);
			if (length == 0) {
				fprintf(stderr, "Null dictionary attribute met.\n");
				exit(EXIT_FAILURE);
			}
			dictitem.value = bdecode(torrent);
			ret = append_btree_dictitem(newbtree, &dictitem);
			if (ret != 0) {
				fprintf(stderr, "Can't append item to dictionary.\n");
				exit(EXIT_FAILURE);
			}
		}
	} else if (c == 'l') {				/* list */
		listitem_t listitem;
		newbtree = new_btree_list(NULL, 0);
		if (newbtree == NULL) {
			fprintf(stderr, "Can't create new list.\n");
			exit(EXIT_FAILURE);
		}
		while ((c = get_torrent_char(torrent)) != 'e') {
			unget_torrent_char(c, torrent);
			// fprintf(stderr, "\tlist(%c)\n", c);
			listitem.element = bdecode(torrent);
			ret = append_btree_listitem(newbtree, &listitem);
			if (ret != 0) {
				fprintf(stderr, "Can't append item to list.\n");
				exit(EXIT_FAILURE);
			}
		}
	} else if (c == 'i') {				/* integer */
		integer_t integer;
		integer = get_torrent_integer(torrent);
		newbtree = new_btree_integer(integer);
	} else if (c >= '0' && c <= '9') {		/* string */
		unget_torrent_char(c, torrent);
		string_t string;
		int length;
		string = get_torrent_string(torrent, &length);
		newbtree = new_btree_string(string, length);
	} else {
		fprintf(stderr, "Unknown bencoded data type, file corrupted!\n");
		exit(EXIT_FAILURE);
	}
	if (newbtree == NULL) {
		perror("Can't create new btree.");
		exit(EXIT_FAILURE);
	}
	return newbtree;
}
