/*
 * file.h
 *
 * $Id: file.h 25 2012-02-19 22:22:24Z diraison $
 *
 * This file is part of the btcheck project (c) 2008-2009 distributed
 * under the GNU GPLv3 license and created by Jean Diraison
 * <jean.diraison@ac-rennes.fr>
 *
 * URL: http://sourceforge.net/projects/btcheck/
 *
 */

#ifndef BTCHECK_FILE_H_
#define BTCHECK_FILE_H_

#include <unistd.h>
#include <limits.h>

#ifndef PATH_MAX
#define PATH_MAX        (4096)
#endif

#include "btree.h"

typedef enum btferr_e btferr_t;

enum btferr_e {
	BTFERR_NONE     = 0,
	BTFERR_TOOSHORT = 1<<0,
	BTFERR_TOOLONG  = 1<<1,
	BTFERR_READ     = 1<<2,
	BTFERR_UNKNOWN  = 1<<3
};

enum readerr_e {
	READERR_NOFILE = -1,
	READERR_HOLES  = -2
};

struct btfile_s {
	int		index;
	btree_t		*list;
	FILE		*file;
	integer_t	size;
#ifdef HAVE_FCHDIR
	int		dirfd;
#else
	char		dirname[PATH_MAX];
#endif
	btferr_t	btferr;
};

typedef struct btfile_s btfile_t;

btfile_t *open_btfile(btree_t *torrentbtree);
size_t read_btfile(btfile_t *btfile, void *data, size_t length);
int close_btfile(btfile_t *btfile);

#endif
