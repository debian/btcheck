/*
 * usage.c
 *
 * $Id: usage.c 62 2017-08-28 21:38:50Z diraison $
 *
 * This file is part of the btcheck project (c) 2008-2009 distributed
 * under the GNU GPLv3 license and created by Jean Diraison
 * <jean.diraison@ac-rennes.fr>
 *
 * URL: http://sourceforge.net/projects/btcheck/
 *
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>

#ifndef PACKAGE_NAME
#define PACKAGE_NAME	"btcheck"
#endif
#ifndef VERSION
#define VERSION		"unknown"
#endif

#ifdef WITH_OPENSSL
#define SHA1_SUPPORT	"OpenSSL crypto library"
#endif
#ifdef WITH_POLARSSL
#define SHA1_SUPPORT	"mbedTLS/PolarSSL crypto library"
#endif
#ifdef WITH_GNUTLS
#define SHA1_SUPPORT	"GnuTLS library"
#endif
#ifdef WITH_GCRYPT
#define SHA1_SUPPORT	"libgcrypt crypto library"
#endif
#ifdef WITH_TOMCRYPT
#define SHA1_SUPPORT	"tomcrypt library"
#endif
#ifdef WITH_NETTLE
#define SHA1_SUPPORT	"Nettle crypto library"
#endif
#ifdef WITH_BEECRYPT
#define SHA1_SUPPORT	"BeeCrypt library"
#endif
#ifdef WITH_KERNEL_CRYPTO_API
#define SHA1_SUPPORT	"Linux Kernel Crypto API"
#endif
#ifdef WITH_WINDOWS_CRYPTOAPI
#define SHA1_SUPPORT	"Windows CryptoAPI"
#endif
#ifndef SHA1_SUPPORT
#define SHA1_SUPPORT	"built-in SHA1 algorithm"
#endif


void version()
{
	fprintf(stderr, "%s version %s (%s) using %s\n", PACKAGE_NAME, VERSION, CANONICAL_HOST, SHA1_SUPPORT);
	exit(EXIT_SUCCESS);
}

void usage(char *progname, int code)
{
	fprintf(stderr, "usage: %s [-h] [-V] [-v] [-q] [-z] [-n] [-i] [-l] file.torrent\n", progname);
	fprintf(stderr, "\t\t-h: show this help.\n");
	fprintf(stderr, "\t\t-V: show version number.\n");
	fprintf(stderr, "\t\t-v: verbose output.\n");
	fprintf(stderr, "\t\t-q: quiet, no output.\n");
	fprintf(stderr, "\t\t-z: fill missing data with zeros.\n");
	fprintf(stderr, "\t\t-n: do not check data (useful whith -i option and default with -l).\n");
	fprintf(stderr, "\t\t-i: show content information of the given torrent file.\n");
	fprintf(stderr, "\t\t-l: list filenames in the given torrent file and don't check data.\n");
	exit(code);
}
