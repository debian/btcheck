/*
 * bdecode.h
 *
 * $Id: bdecode.h 34 2012-08-08 21:52:21Z diraison $
 *
 * This file is part of the btcheck project (c) 2008-2009 distributed
 * under the GNU GPLv3 license and created by Jean Diraison
 * <jean.diraison@ac-rennes.fr>
 *
 * URL: http://sourceforge.net/projects/btcheck/
 *
 */

#ifndef BTCHECK_BDECODE_H_
#define BTCHECK_BDECODE_H_

#include "btree.h"

btree_t *bdecode(FILE *torrent);

#endif
