/*
 * btree.h
 *
 * $Id: btree.h 43 2015-07-07 00:29:33Z diraison $
 *
 * This file is part of the btcheck project (c) 2008-2009 distributed
 * under the GNU GPLv3 license and created by Jean Diraison
 * <jean.diraison@ac-rennes.fr>
 *
 * URL: http://sourceforge.net/projects/btcheck/
 *
 */

#ifndef BTCHECK_BTREE_H_
#define BTCHECK_BTREE_H_

#if defined(_MSC_VER) || defined(__MINGW32__) || defined(__MINGW64__)
#define LLD_FMT "%I64d"
#else
#define LLD_FMT "%lld"
#endif

#define MAX_ITEM_NUMBER		(1000000)
#define MAX_STRING_LENGTH	(1<<25)
#define MAX_INTEGER_VALUE	(900000000000000000LL)	/* Lower than 2^63/10 */
#define MAX_INTEGER_DIGITS	(20)		/* MAX_INTEGER_VALUE < 10^MAX_INTEGER_DIGITS */

typedef long long integer_t;
typedef char *string_t;
typedef struct btree_s btree_t;
typedef struct dictitem_s dictitem_t;
typedef struct listitem_s listitem_t;
typedef enum btype_e btype_t;

enum btype_e {
	BTYPE_UNKNOWN    = 0,
	BTYPE_INTEGER    = 1,
	BTYPE_STRING     = 2,
	BTYPE_DICTIONARY = 3,
	BTYPE_LIST       = 4
};

struct dictitem_s {
	string_t	attribute;
	btree_t		*value;
};

struct listitem_s {
	btree_t		*element;
};

struct btree_s {
	btype_t		type;
	int		length;
	union {
		integer_t	integer;
		string_t	string;
		dictitem_t	*dictionary;
		listitem_t	*list;
	} data;
};

btype_t get_btree_type(btree_t *btree);

int get_btree_length(btree_t *btree);

int get_btree_integer(btree_t *btree, integer_t *integer);
int get_btree_string(btree_t *btree, string_t *string, int *stringlength);

btree_t *get_list_element(btree_t *btree, int index);

string_t get_dict_attribute(btree_t *btree, int index);
btree_t *get_dict_value(btree_t *btree, int index);
btree_t *search_dict_value(btree_t *btree, string_t info[]);

btree_t *new_btree_integer(integer_t integer);
btree_t *new_btree_string(string_t string, int length);
btree_t *new_btree_dict(dictitem_t *dict, int length);
btree_t *new_btree_list(listitem_t *list, int length);

int append_btree_dictitem(btree_t *btree, dictitem_t *dictitem);
int append_btree_listitem(btree_t *btree, listitem_t *listitem);

void free_btree(btree_t *btree);

void *dump_btree(btree_t *btree, int level);

int map_list(btree_t *btree, int (*func)(void *, btree_t *), void *ctx);

#endif
