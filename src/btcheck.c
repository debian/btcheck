/*
 * btcheck.c
 *
 * $Id: btcheck.c 61 2017-08-28 14:46:13Z diraison $
 *
 * This file is part of the btcheck project (c) 2008-2009 distributed
 * under the GNU GPLv3 license and created by Jean Diraison
 * <jean.diraison@ac-rennes.fr>
 *
 * URL: http://sourceforge.net/projects/btcheck/
 *
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "btree.h"
#include "bdecode.h"
#include "info.h"
#include "bencode.h"
#include "check.h"
#include "list.h"
#include "usage.h"

int Verbose;
int SkipCheck;
int ZeroFill;
int PrintInfo;
int PrintFileList;

static void fprint_btree(FILE *file, int c)
{
	fprintf(file, "%c", c);
}

int main(int argc, char *argv[])
{
        extern char *optarg;
        extern int optind, opterr, optopt;
	btree_t *torrentbtree;
	FILE *torrent;
	char *filename = NULL;
	int c, failure = 0;

	while ((c = getopt(argc, argv, "hVvqnizl")) != EOF) {
		switch(c) {
		case 'h': usage(argv[0], EXIT_SUCCESS);
		case 'V': version();
		case 'v': Verbose++;
			  break;
		case 'q': Verbose--;
			  break;
		case 'i': PrintInfo = 1;
			  break;
		case 'z': ZeroFill = 1;
			  break;
		case 'l': PrintFileList = 1;
		case 'n': SkipCheck = 1;
			  break;
		default:  usage(argv[0], EXIT_FAILURE);
		}
	}
	if (argc == (optind + 1)) {
		filename = argv[optind];
	} else {
		usage(argv[0], EXIT_FAILURE);
	}

	torrent = fopen(filename, "rb");
	if (torrent == NULL) {
		perror("Can't open torrent file.");
		exit(EXIT_FAILURE);
	}

	torrentbtree = bdecode(torrent);
	// dump_btree(torrentbtree, 0);
	
	if (PrintInfo)
		print_info(torrentbtree);
	// bencode(torrentbtree, (void *)fprint_btree, (void *)stdout);
	
	if (PrintFileList)
		print_files_list(torrentbtree);
	
	if (SkipCheck == 0)
		failure = check(torrentbtree);
		
	free_btree(torrentbtree);
	
	fclose(torrent);

	exit(failure == 0 ? EXIT_SUCCESS : EXIT_FAILURE);
}
