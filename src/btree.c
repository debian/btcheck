/*
 * btree.c
 *
 * $Id: btree.c 26 2012-02-19 22:28:17Z diraison $
 *
 * This file is part of the btcheck project (c) 2008-2009 distributed
 * under the GNU GPLv3 license and created by Jean Diraison
 * <jean.diraison@ac-rennes.fr>
 *
 * URL: http://sourceforge.net/projects/btcheck/
 *
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "btree.h"

btype_t get_btree_type(btree_t *btree)
{
	if (btree == NULL)
		return BTYPE_UNKNOWN;
	return btree->type;
}

int get_btree_length(btree_t *btree)
{
	if (btree == NULL || btree->type == BTYPE_INTEGER)
		return -1;
	return btree->length;
}

btree_t *get_list_element(btree_t *btree, int index)
{
	if (btree == NULL || btree->type != BTYPE_LIST)
		return NULL;
	if (index < 0 || index >= btree->length)
		return NULL;
	return btree->data.list[index].element;
}

string_t get_dict_attribute(btree_t *btree, int index)
{
	if (btree == NULL || btree->type != BTYPE_DICTIONARY)
		return NULL;
	if (index < 0 || index >= btree->length)
		return NULL;
	return btree->data.dictionary[index].attribute;
}

btree_t *get_dict_value(btree_t *btree, int index)
{
	if (btree == NULL || btree->type != BTYPE_DICTIONARY)
		return NULL;
	if (index < 0 || index >= btree->length)
		return NULL;
	return btree->data.dictionary[index].value;
}

int get_btree_integer(btree_t *btree, integer_t *integer)
{
	if (btree == NULL || integer == NULL || btree->type != BTYPE_INTEGER)
		return -1;
	*integer = btree->data.integer;
	return 0;
}

int get_btree_string(btree_t *btree, string_t *string, int *stringlength)
{
	if (btree == NULL || string == NULL || btree->type != BTYPE_STRING)
		return -1;
	if (stringlength != NULL)
		*stringlength = btree->length;
	*string = btree->data.string;
	return 0;
}

btree_t *new_btree_integer(integer_t integer)
{
	btree_t *newbtree;
	newbtree = malloc(sizeof(btree_t));
	if (newbtree == NULL)
		return NULL;
	newbtree->type         = BTYPE_INTEGER;
	newbtree->length       = 0;
	newbtree->data.integer = integer;
	return newbtree;
}

btree_t *new_btree_string(string_t string, int length)
{
	btree_t *newbtree;
	newbtree = malloc(sizeof(btree_t));
	if (newbtree == NULL)
		return NULL;
	newbtree->type        = BTYPE_STRING;
	newbtree->length      = length;
	newbtree->data.string = string;
	return newbtree;
}

btree_t *new_btree_dict(dictitem_t *dict, int length)
{
	btree_t *newbtree;
	newbtree = malloc(sizeof(btree_t));
	if (newbtree == NULL)
		return NULL;
	newbtree->type            = BTYPE_DICTIONARY;
	newbtree->length          = length;
	newbtree->data.dictionary = dict;
	return newbtree;
}

btree_t *new_btree_list(listitem_t *list, int length)
{
	btree_t *newbtree;
	newbtree = malloc(sizeof(btree_t));
	if (newbtree == NULL) {
		fprintf(stderr, "Can't create new btree for list.\n");
		exit(EXIT_FAILURE);
	}
	newbtree->type      = BTYPE_LIST;
	newbtree->length    = length;
	newbtree->data.list = list;
	return newbtree;
}

int append_btree_dictitem(btree_t *btree, dictitem_t *dictitem)
{
	dictitem_t *newdict;
	if (btree == NULL || btree->type != BTYPE_DICTIONARY || btree->length >= MAX_ITEM_NUMBER)
		return -1;
	newdict = realloc(btree->data.dictionary, sizeof(dictitem_t)*(btree->length+1));
	if (newdict == NULL) {
		fprintf(stderr, "Can't increase dictionary length.\n");
		return -1;
	}
	newdict[btree->length].attribute = dictitem->attribute;
	newdict[btree->length].value     = dictitem->value;
	btree->data.dictionary = newdict;
	btree->length++;
	return 0;
}

int append_btree_listitem(btree_t *btree, listitem_t *listitem)
{
	listitem_t *newlist;
	if (btree == NULL || btree->type != BTYPE_LIST || btree->length >= MAX_ITEM_NUMBER)
		return -1;
	newlist = realloc(btree->data.list, sizeof(listitem_t)*(btree->length+1));
	if (newlist == NULL) {
		fprintf(stderr, "Can't increase list length.\n");
		return -1;
	}
	newlist[btree->length].element = listitem->element;
	btree->data.list = newlist;
	btree->length++;
	return 0;
}

btree_t *search_dict_value(btree_t *btree, string_t info[])
{
	string_t attribute;
	int index;
	if (btree == NULL || info == NULL)
		return NULL;
	if (info[0] == NULL)
		return btree;
	if (btree->type == BTYPE_DICTIONARY) {
		for(index = 0; index < btree->length; index++) {
			attribute = btree->data.dictionary[index].attribute;
			if (strlen((char *)attribute) == strlen((char *)info[0]) && strncmp((char *)attribute, (char *)info[0], strlen((char *)info[0])) == 0) {
				btree = btree->data.dictionary[index].value;
				return search_dict_value(btree, &info[1]);
			}
		}
	}
	return NULL;
}

void free_btree(btree_t *btree)
{
	int i;
	if (btree !=  NULL) {
		switch (btree->type) {
		case BTYPE_INTEGER:	break;
		case BTYPE_STRING:	free(btree->data.string);
					break;
		case BTYPE_DICTIONARY:	for(i = 0; i < btree->length; i++) {
						free(btree->data.dictionary[i].attribute);
						free_btree(btree->data.dictionary[i].value);
					}
					free(btree->data.dictionary);
					break;
		case BTYPE_LIST:	for(i = 0; i < btree->length; i++) {
						free_btree(btree->data.list[i].element);
					}
					free(btree->data.list);
					break;
		default:	fprintf(stderr, "Can't free btree, unknown type!\n");
				exit(EXIT_FAILURE);
		}
		free(btree);
	}
}

static inline void tab(int level) { while (--level >= 0) fprintf(stderr, "\t"); }

void *dump_btree(btree_t *btree, int level)
{
	int i;
	if (btree == NULL)
		return NULL;
	switch (btree->type) {
	case BTYPE_INTEGER:	tab(level);
				fprintf(stderr, "integer: " LLD_FMT "\n", (long long)btree->data.integer);
				break;
	case BTYPE_STRING:	tab(level);
				fprintf(stderr, "string(%d): %s\n", btree->length, (char *)btree->data.string);
				break;
	case BTYPE_DICTIONARY:	for (i = 0; i < btree->length; i++) {
					tab(level);
					fprintf(stderr, "dict(attribute[%d/%d]): %s\n", i, btree->length-1, (char *)btree->data.dictionary[i].attribute);
					tab(level);
					fprintf(stderr, "dict(value[%d/%d]):\n", i, btree->length-1);
					dump_btree(btree->data.dictionary[i].value, level+1);
				}
				break;
	case BTYPE_LIST:	for (i = 0; i < btree->length; i++) {
					tab(level);
					fprintf(stderr, "list(element[%d/%d]):\n", i, btree->length-1);
					dump_btree(btree->data.list[i].element, level+1);
				}
				break;
	default:		fprintf(stderr, "Unknown btree type.\n");
				exit(EXIT_FAILURE);
	}
	return NULL;
}

int map_list(btree_t *btree, int (*func)(void *, btree_t *), void *ctx)
{
	int i, ret;
	if (btree == NULL || btree->type != BTYPE_LIST)
		return -1;
	for (i = 0; i < btree->length; i++) {
		ret = func(ctx, btree->data.list[i].element);
		if (ret != 0)
			return -1;
	}
	return 0;
}
