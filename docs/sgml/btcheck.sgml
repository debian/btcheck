<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook V4.1//EN">
<!--
btcheck.sgml

$Id: btcheck.sgml 42 2015-05-31 21:50:09Z diraison $

This file is part of the btcheck project (c) 2008-2009 distributed
under the GNU GPLv3 license and created by Jean Diraison
<jean.diraison@ac-rennes.fr>

URL: http://sourceforge.net/projects/btcheck/
-->
<refentry id="btcheck">
    <refmeta>
        <refentrytitle>btcheck</refentrytitle>
        <manvolnum>1</manvolnum>
    </refmeta>
    <refnamediv>
        <refname>btcheck</refname>
            <refpurpose>
                Bittorrent downloaded data checker
            </refpurpose>
    </refnamediv>
    <refsynopsisdiv>
        <cmdsynopsis>
            <command>btcheck</command>
		<arg choice="opt">-h</arg>
		<arg choice="opt">-V</arg>
		<arg choice="opt">-v</arg>
		<arg choice="opt">-q</arg>
		<arg choice="opt">-n</arg>
		<arg choice="opt">-z</arg>
                <arg choice="opt">-i</arg>
                <arg choice="opt">-l</arg>
		<arg choice="req">file.torrent</arg>
        </cmdsynopsis>
    </refsynopsisdiv>
    <refsect1>
        <title>DESCRIPTION</title>
        <para>
	    <command>btcheck</command>(1) is a tool to list torrent file content (torrent hash value, tracker announce, comments, files list with sizes) and to check hashes of downloaded data associated with this torrent file.
        </para>
    </refsect1>
    <refsect1>
        <title>OPTIONS</title>
        <variablelist>
            <varlistentry>
                <term>-h</term>
                <listitem>
                    <para>
                        Show summary of options and exit.
                    </para>
                </listitem>
	    </varlistentry>
	    <varlistentry>
	        <term>-V</term>
		<listitem>
		    <para>
			Show the current version number and exit.
		    </para>
		</listitem>
	    </varlistentry>
	    <varlistentry>
                <term>-v</term>
                <listitem>
                    <para>
                        Verbose output (can be used many times to increase verbosity level).
                    </para>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term>-q</term>
                <listitem>
                    <para>
                        Quiet output, check success or failure is set in return code.
                    </para>
                </listitem>
            </varlistentry>
	    <varlistentry>
	        <term>-n</term>
		<listitem>
		    <para>
		        Do not check data (useful with -i option and default with -l).
		    </para>
		</listitem>
	    </varlistentry>
	    <varlistentry>
		<term>-z</term>
		<listitem>
		    <para>
			Fill missing data with zeros when checking (this forces hash computation).
		    </para>
		</listitem>
	    </varlistentry>
	    <varlistentry>
                <term>-i</term>
                <listitem>
                    <para>
                        Show the content of the given torrent file. This includes the files list with their size, the tracker announce, the torrent hash value.
                    </para>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term>-l</term>
                <listitem>
                    <para>
			List filenames with size of the given torrent file and don't check data.
                    </para>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term>file.torrent</term>
                <listitem>
                    <para>
                        The torrent file to use.
                    </para>
                </listitem>
            </varlistentry>
        </variablelist>
    </refsect1>
    <refsect1>
        <title>EXAMPLES</title>
        <para>
            Here is some examples.
        </para>
        <example>
            <title>default usage (checking mode)</title>
            <programlisting>
    btcheck foo.torrent
            </programlisting>
        </example>
        <example>
	<title>list files declared</title>
            <programlisting>
    btcheck -l bar.torrent
            </programlisting>
        </example>
    </refsect1>
    <refsect1>          
        <title>NOTE</title>
        <para>  
             This program tries to check data in the directory declared in torrent file. If this directory doesn't exist, check is done in the current directory.
        </para>
    </refsect1> 
    <refsect1>
        <title>BUGS</title>
        <para>
             None as far as I know.
        </para>
    </refsect1>
    <refsect1>
        <title>AUTHOR</title>
        <para>
            <author>
                <firstname>Jean </firstname>
                <surname>Diraison</surname>
            </author>
            <address><email>jean.diraison@ac-rennes.fr</email></address>
        </para>
    </refsect1>
    <refsect1>
        <title>AVAILABILITY</title>
        <para>
	    This tool can be download at <ulink url="http://btcheck.sourceforge.net/">http://btcheck.sourceforge.net/</ulink>. It is distributed under the terms of the GPLv3 License (http://www.gnu.org/licenses/gpl-3.0.txt).
        </para>
    </refsect1>
    <refsect1>
        <title>SEE ALSO</title>
            <para>
                <command>btshowmetainfo</command>(1)
            </para>
    </refsect1>
</refentry>

